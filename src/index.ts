import './scss/index.scss';

import _ from 'lodash';
import printMe from './print';

function component() {
  var element = document.createElement('div');
  var btn = document.createElement('button');

  element.innerHTML = _.join(['Hello', 'webpack'], ' ');

  btn.innerHTML = 'Click this and check the console!';
  btn.onclick = () => { printMe(); };

  element.appendChild(btn);

  return element;
}

document.body.getElementsByClassName('page-content')[0].appendChild(component());

declare const $: any;

$(document).ready(() => {
  $('body').on('click', '[data-toggle-class]', ({ target }: { target: Element }) => {
    const button = $(target).closest('[data-toggle-class]');
    const toggleTarget = button.attr('data-target')
      ? $(button.attr('data-target'))
      : button;
    toggleTarget.toggleClass(button.attr('data-toggle-class'));
  });
})
